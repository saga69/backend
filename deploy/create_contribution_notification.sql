-- Deploy saga-backend:create_contribution_notification to pg

BEGIN;

  -- trigger function for contribution notification
  -- Precondition: TG_OP = 'INSERT'
  CREATE FUNCTION backend.fn_contributions_notification()
  RETURNS trigger
  LANGUAGE plpgsql
  AS $$
    BEGIN
      PERFORM pg_notify(
        'postgres-websockets-listener',
        json_build_object(
          'channel', 'contributions',
          'payload', 'new contribution'
        )::text
      );
      RETURN null;
    END;
  $$;

  CREATE TRIGGER tg_contributions_3
  AFTER INSERT ON backend.t_contributions
  FOR EACH ROW EXECUTE PROCEDURE backend.fn_contributions_notification();

COMMIT;
