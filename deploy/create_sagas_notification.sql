-- Deploy saga-backend:create_sagas_notification to pg

BEGIN;

  -- trigger function for saga notification
  -- Precondition: TG_OP = 'INSERT'
  CREATE FUNCTION backend.fn_sagas_notification()
  RETURNS trigger
  LANGUAGE plpgsql
  AS $$
    BEGIN
      PERFORM pg_notify(
        'postgres-websockets-listener',
        json_build_object(
          'channel', 'sagas-' || NEW.saga_id,
          'payload', 'new notification'
        )::text
      );
      RETURN null;
    END;
  $$;

  CREATE TRIGGER tg_contributions_4
  AFTER INSERT ON backend.t_contributions
  FOR EACH ROW EXECUTE PROCEDURE backend.fn_sagas_notification();

COMMIT;
