-- Deploy saga-backend:create_contributions_view to pg

BEGIN;

  -- Contirbutions view acts as a facade around backend.t_contributions
  CREATE VIEW api.contributions AS
    SELECT * FROM backend.t_contributions;

  GRANT SELECT ON api.contributions TO api_anon;
  GRANT INSERT ON api.contributions TO api_anon;
  GRANT TRIGGER ON api.contributions TO api_anon;
  GRANT USAGE, SELECT ON SEQUENCE backend.t_contributions_id_seq TO api_anon;

COMMIT;
