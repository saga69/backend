-- Deploy saga-backend:add_saga_foreign_key_to_contributions to pg

BEGIN;

  -- Function used to select the first saga
  CREATE FUNCTION backend.fn_first_t_saga()
  RETURNS INTEGER
  LANGUAGE plpgsql
  AS $$
    DECLARE
      saga_id INTEGER;
      errors JSONB;
    BEGIN
      SELECT id INTO saga_id FROM backend.t_sagas LIMIT 1;
      IF saga_id IS NULL THEN
        RAISE EXCEPTION '%', jsonb_set(errors, '{errors,saga_id}', '"no sagas to reference"')::text;
      END IF;
      RETURN saga_id;
    END;
  $$;

  -- Adding single saga if none already exist
  DO
  $$
    BEGIN
      IF EXISTS(SELECT FROM backend.t_sagas) THEN
        INSERT INTO backend.t_sagas (title)
        VALUES ('No Name');
      END IF;
    END;
  $$;

  -- Adding column contain data
  ALTER TABLE backend.t_contributions
  ADD COLUMN saga_id INTEGER NOT NULL
  DEFAULT backend.fn_first_t_saga();


  -- Adding reference to t_saga table
  ALTER TABLE backend.t_contributions
  ADD CONSTRAINT fk_saga_id
  FOREIGN KEY (saga_id)
  REFERENCES backend.t_sagas(id);

  -- Crreate function for validation in t_contributions
  CREATE OR REPLACE FUNCTION backend.fn_contributions_validation()
  RETURNS TRIGGER
  SECURITY DEFINER
  LANGUAGE plpgsql AS $$
    DECLARE
      errors JSONB;
    BEGIN
      SELECT '{ "errors": {} }' INTO errors;

      -- parent_id validations
      IF NEW.parent_id IS NOT NULL AND NEW.saga_id IS NOT NULL AND NOT EXISTS (
        SELECT FROM backend.t_contributions
        WHERE id = NEW.parent_id
        AND saga_id = NEW.saga_id
      ) THEN
        SELECT jsonb_set(errors, '{errors,parent_id}', '"does not exist"')
        INTO errors;
      END IF;

      -- saga_id validations
      IF NEW.saga_id IS NULL THEN
        SELECT jsonb_set(errors, '{errors,saga_id}', '"shall not be empty"')
        INTO errors;
      END IF;

      -- content validations
      IF NEW.content IS NULL OR NEW.content = '' THEN
        SELECT jsonb_set(errors, '{errors,content}', '"shall not be empty"')
        INTO errors;
      END IF;

      -- finalizing validation
      IF (SELECT COUNT(*) FROM jsonb_object_keys(errors->'errors')) > 0 THEN
        RAISE EXCEPTION '%', errors::text;
      ELSE
        RETURN NEW;
      END IF;
    END;
  $$;

  -- update contributions view
  CREATE OR REPLACE VIEW api.contributions AS
    SELECT * FROM backend.t_contributions;

COMMIT;
