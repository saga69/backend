-- Deploy saga-backend:create_api_anon_role to pg

BEGIN;

  CREATE ROLE api_anon NOLOGIN;

  GRANT USAGE ON SCHEMA api TO api_anon;

COMMIT;
