-- Deploy saga-backend:create_contributions_table to pg

BEGIN;

  -- Contributions table definition
  CREATE TABLE backend.t_contributions (
    id          SERIAL     PRIMARY KEY,
    parent_id   INTEGER REFERENCES backend.t_contributions(id),
    content     TEXT                     NOT NULL,
    created_at  TIMESTAMP WITH TIME ZONE NOT NULL
  );

  -- Pre-insertion trigger
  -- Sets created_at to transaction's execution time
  CREATE FUNCTION backend.fn_contributions_pre_insert()
  RETURNS TRIGGER
  LANGUAGE plpgsql AS $$
    BEGIN
      SELECT now() INTO NEW.created_at;
      RETURN NEW;
    END;
  $$;

  -- Validation function definition
  -- This function will raise an exception in case validation does not pass
  -- Precondition: TG_OP = 'INSERT'
  -- Errors format:
  -- {
  --   "errors": {
  --     "parent_id": <ERROR TEXT>,
  --     "content": <ERROR TEXT>,
  --   }
  -- }
  CREATE FUNCTION backend.fn_contributions_validation()
  RETURNS TRIGGER
  SECURITY DEFINER
  LANGUAGE plpgsql AS $$
    DECLARE
      errors JSONB;
    BEGIN
      SELECT '{ "errors": {} }' INTO errors;

      -- parent_id validations
      IF NEW.parent_id IS NOT NULL AND NOT EXISTS (
        SELECT 42
        FROM backend.t_contributions
        WHERE id = NEW.parent_id
      ) THEN
        SELECT jsonb_set(errors, '{errors,parent_id}', '"does not exist"')
        INTO errors;
      END IF;

      -- content validations
      IF NEW.content IS NULL OR NEW.content = '' THEN
        SELECT jsonb_set(errors, '{errors,content}', '"shall not be empty"')
        INTO errors;
      END IF;

      -- finalizing validation
      IF (SELECT COUNT(*) FROM jsonb_object_keys(errors->'errors')) > 0 THEN
        RAISE EXCEPTION '%', errors::text;
      ELSE
        RETURN NEW;
      END IF;
    END;
  $$;

  CREATE TRIGGER tg_contributions_1
    BEFORE INSERT ON backend.t_contributions
    FOR EACH ROW EXECUTE PROCEDURE backend.fn_contributions_pre_insert();

  CREATE TRIGGER tg_contributions_2
    BEFORE INSERT ON backend.t_contributions
    FOR EACH ROW EXECUTE PROCEDURE backend.fn_contributions_validation();

COMMIT;
