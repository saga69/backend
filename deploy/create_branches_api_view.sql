-- Deploy saga-backend:create_branches_api_view to pg

BEGIN;

  CREATE VIEW api.branches AS SELECT * FROM backend.v_branches;

  GRANT SELECT ON api.branches TO api_anon;

COMMIT;
