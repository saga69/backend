-- Deploy saga-backend:create_branches_view to pg

BEGIN;

  -- This view presents a list of branches that can be deduced from contribution
  -- graphs across all of the sagas. A branch in this case is not exactly what
  -- a branch is in its natural sense, that is - when a new "branch" is created
  -- by adding a contribution as child of another contribution, that already has
  -- children, it actually creates two new branches. To illustrate:
  --
  --   A          >     A  \
  --   |          >     |   > branch #1
  --   B          >     B  /
  --   |          >    / \
  --   C          >   C   D < new contribution
  --   ^          >   ^   ^
  --   branch #1  >   branches #2 and #3
  --
  -- Formally, a branch is essentially represented by two properties: the id of
  -- its first contribution (the head of the branch) and the id of the last
  -- contribution (the tail of the branch). The head of the branch is a
  -- contribution whose parent contribution does not have a single child.
  -- The tail of the branch is the first contribution that does not have a sngle
  -- child. In fact, to get a set of branches out of contributions graph, it is
  -- enough to squash sequential non-branching contributions into "one". For
  -- instance, for the following contribution graph (@ represents the non-existing
  -- root, as the first contribution in a saga has NULL parent_id):
  --
  --   @---A---B---C---D---E---F
  --            \
  --             G---H---I---J
  --                      \
  --                       K---L
  --
  -- The following branch graph should be rendered (branch_head,branch_tail):
  --
  --   @---(A,B)---(C,F)
  --          \
  --         (G,I)---(J,J)
  --            \
  --           (K,L)
  --
  -- The requirements for this view, aside from deducing the heads and the tails
  -- of all of the branches, are:
  -- * the amount of contributions in a branch
  -- * the content of the branch's head contribution
  -- * the pointer to the parent branch
  -- * list or contribution ids that are part of the branch
  -- * list of contribution contents that are part of the branch
  CREATE VIEW backend.v_branches AS
    WITH RECURSIVE contributions_with_child_counts(
      id, parent_id, saga_id, child_count, content
    ) AS (
      SELECT c.id
           , c.parent_id
           , c.saga_id
           , (
              SELECT COUNT(c1)
              FROM backend.t_contributions c1
              WHERE c1.parent_id = c.id
             )
           , c.content
        FROM backend.t_contributions c
    ), branches (
      contribution_id, contribution_parent_id, saga_id,
      parent_tail, prev_child_count,
      contribution_ids,
      contribution_contents
    ) AS (
      -- induction base definition
      SELECT c.id
           , c.parent_id
           , c.saga_id
           , null::int
           , child_count
           , jsonb_build_array(c.id)
           , jsonb_build_array(c.content)
        FROM contributions_with_child_counts c
       WHERE c.parent_id IS NULL

       UNION ALL

      -- induction step definition
      SELECT c.id
           , c.parent_id
           , c.saga_id
           , CASE WHEN b.prev_child_count = 1 THEN b.parent_tail ELSE b.contribution_id END
           , c.child_count
           , CASE WHEN b.prev_child_count = 1 THEN
               b.contribution_ids || jsonb_build_array(c.id)
             ELSE
               jsonb_build_array(c.id)
             END
           , CASE WHEN b.prev_child_count = 1 THEN
               b.contribution_contents || jsonb_build_array(c.content)
             ELSE
               jsonb_build_array(c.content)
             END
        FROM branches b
        JOIN contributions_with_child_counts c
          ON c.parent_id = b.contribution_id
    ) SELECT b.parent_tail
           , (b.contribution_ids->0)::INT AS head
           , (b.contribution_ids->-1)::INT AS tail
           , jsonb_array_length(b.contribution_ids) AS contribution_count
           , b.saga_id
           , b.contribution_contents->>0 AS head_content
           , b.contribution_ids
           , b.contribution_contents
        FROM branches b
       WHERE prev_child_count <> 1;

COMMIT;
