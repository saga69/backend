-- Deploy saga-backend:create_sagas_view to pg

BEGIN;

  CREATE VIEW api.sagas AS
    SELECT * FROM backend.t_sagas;


  GRANT SELECT ON api.sagas TO api_anon;
  GRANT INSERT ON api.sagas TO api_anon;
  GRANT TRIGGER ON api.sagas TO api_anon;
  GRANT USAGE, SELECT ON SEQUENCE backend.t_sagas_id_seq TO api_anon;
  GRANT SELECT ON backend.t_sagas TO api_anon;

COMMIT;
