-- Deploy saga-backend:create_auth_schema to pg

BEGIN;

  CREATE SCHEMA auth;

  GRANT USAGE ON SCHEMA auth TO api_anon;

COMMIT;
