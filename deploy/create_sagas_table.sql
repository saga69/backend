-- Deploy saga-backend:create_sagas_table to pg

BEGIN;

  -- SAGAs table definition
  CREATE TABLE backend.t_sagas (
    id         SERIAL    PRIMARY KEY,
    title      TEXT      NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL
  );

  -- Pre-insertion trigger
  -- Sets created_at to transactions execution time
  CREATE FUNCTION backend.fn_sagas_pre_insert()
  RETURNS TRIGGER
  LANGUAGE plpgsql AS $$
    BEGIN
      SELECT now() INTO NEW.created_at;
      RETURN NEW;
    END;
  $$;

  -- Validation function definition
  -- This function will raise an exception in case validation does not pass
  -- Precondition: TG_OP = 'INSERT'
  -- Errors format:
  -- {
  --   "errors": {
  --     "parent_id": <ERROR TEXT>,
  --     "content": <ERROR TEXT>
  --    }
  -- }
  CREATE FUNCTION backend.fn_sagas_validation()
  RETURNS TRIGGER
  LANGUAGE plpgsql AS $$
    DECLARE
      errors JSONB;
    BEGIN
      SELECT '{ "errors": {} }' INTO errors;

      -- content validations
      IF NEW.title IS NULL OR NEW.title = '' THEN
        SELECT jsonb_set(errors, '{errors,title}', '"shall not be empty"')
        INTO errors;
      END IF;

      -- finalizing validation
      IF (SELECT COUNT(*) FROM jsonb_object_keys(errors->'errors')) > 0 THEN
        RAISE EXCEPTION '%', errors::text;
      ELSE
        RETURN NEW;
      END IF;
    END;
  $$;

  CREATE TRIGGER tg_sagas_1
    BEFORE INSERT ON backend.t_sagas
    FOR EACH ROW EXECUTE PROCEDURE backend.fn_sagas_pre_insert();

  CREATE TRIGGER tg_sagas_2
    BEFORE INSERT ON backend.t_sagas
    FOR EACH ROW EXECUTE PROCEDURE backend.fn_sagas_validation();

COMMIT;
