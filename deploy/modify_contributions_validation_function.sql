-- Deploy saga-backend:modify_contributions_validation_function to pg

BEGIN;

  -- Validation function definition
  -- This function will raise an exception in case validation does not pass
  -- Precondition: TG_OP = 'INSERT'
  -- Errors format:
  -- {
  --   "errors": {
  --     "parent_id": <ERROR TEXT>,
  --     "content": <ERROR TEXT>,
  --   }
  -- }
  CREATE OR REPLACE FUNCTION backend.fn_contributions_validation()
  RETURNS TRIGGER
  SECURITY DEFINER
  LANGUAGE plpgsql AS $$
    DECLARE
      errors JSONB;
    BEGIN
      SELECT '{ "errors": {} }' INTO errors;

      -- parent_id validations
      IF NEW.parent_id IS NOT NULL AND NOT EXISTS (
        SELECT 42
        FROM backend.t_contributions
        WHERE id = NEW.parent_id
      ) THEN
        SELECT jsonb_set(errors, '{errors,parent_id}', '"does not exist"')
        INTO errors;
      END IF;

      IF ((SELECT COUNT(*) FROM backend.t_contributions
          WHERE saga_id = NEW.saga_id
          AND parent_id IS NULL) = 1 AND NEW.parent_id IS NULL) THEN
        SELECT jsonb_set(errors, '{errors,parent_id}', '"has multiple root contributions"')
        INTO errors;
      END IF;

      -- content validations
      IF NEW.content IS NULL OR NEW.content = '' THEN
        SELECT jsonb_set(errors, '{errors,content}', '"shall not be empty"')
        INTO errors;
      END IF;

      -- finalizing validation
      IF (SELECT COUNT(*) FROM jsonb_object_keys(errors->'errors')) > 0 THEN
        RAISE EXCEPTION '%', errors::text;
      ELSE
        RETURN NEW;
      END IF;
    END;
  $$;

COMMIT;
