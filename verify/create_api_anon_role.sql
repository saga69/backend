-- Verify saga-backend:create_api_anon_role on pg

BEGIN;

  DO $$
    BEGIN
      ASSERT EXISTS(
        SELECT rolname
        FROM pg_roles
        WHERE rolname = 'api_anon'
      );

      ASSERT NOT (SELECT rolcanlogin FROM pg_roles WHERE rolname = 'api_anon');
    END;
  $$;

  DO $$
    BEGIN
      ASSERT has_schema_privilege('api_anon', 'api', 'usage');
      ASSERT NOT has_schema_privilege('api_anon', 'api', 'create');

      ASSERT NOT has_schema_privilege('api_anon', 'backend', 'create');
      ASSERT NOT has_schema_privilege('api_anon', 'backend', 'usage');
    END;
  $$;

ROLLBACK;
