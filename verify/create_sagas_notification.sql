-- Verify saga-backend:create_sagas_notification on pg

BEGIN;

  -- test that insert still works
  INSERT INTO backend.t_sagas (title)
  VALUES ('some content');

  -- test that api insert still works
  SET ROLE api_anon;
  INSERT INTO api.sagas (title)
  VALUES ('yo here');

ROLLBACK;
