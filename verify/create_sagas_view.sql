-- Verify saga-backend:create_sagas_view on pg

BEGIN;

  DO $$
    BEGIN
      ASSERT has_table_privilege('api_anon', 'api.sagas', 'select');
      ASSERT has_table_privilege('api_anon', 'api.sagas', 'insert');
      ASSERT NOT has_table_privilege('api_anon', 'api.sagas', 'update');
      ASSERT NOT has_table_privilege('api_anon', 'api.sagas', 'delete');
      ASSERT NOT has_table_privilege('api_anon', 'api.sagas', 'truncate');
      ASSERT NOT has_table_privilege('api_anon', 'api.sagas', 'references');
      ASSERT has_table_privilege('api_anon', 'api.sagas', 'trigger');
    END;
  $$;

  -- testing select
  INSERT INTO backend.t_sagas (title) VALUES ('Why hello there');

  SET ROLE api_anon;

  DO $$ BEGIN ASSERT (SELECT COUNT(*) FROM api.sagas) = 1; END; $$;

  -- testing insert
  INSERT INTO api.sagas (title) VALUES ('New story');

  DO $$ BEGIN ASSERT (SELECT COUNT(*) FROM api.sagas) = 2; END; $$;

  -- test insert of null title throws exception
  DO $$
    BEGIN
      INSERT INTO api.sagas (title) VALUES (NULL);

    EXCEPTION WHEN OTHERS THEN
      ASSERT (sqlerrm LIKE '%shall not be empty%');
    END;
  $$;

  DO $$
    DECLARE
      saga_id_orig INTEGER;
      saga_id_new INTEGER;
      contribution_parent INTEGER;
    BEGIN
      INSERT INTO api.sagas (title) VALUES ('This is a story') RETURNING id INTO saga_id_orig;
      INSERT INTO api.sagas (title) VALUES ('This is also a story') RETURNING id INTO saga_id_new;
      INSERT INTO api.contributions (saga_id, parent_id, content) VALUES (saga_id_orig, NULL, 'the saga continues') RETURNING id INTO contribution_parent;
      INSERT INTO api.contributions (saga_id, parent_id, content) VALUES (saga_id_new, contribution_parent, 'uh oh');
      RAISE EXCEPTION '%', 'this line shall not be reached';
    EXCEPTION WHEN OTHERS THEN
    END;
  $$;

ROLLBACK;
