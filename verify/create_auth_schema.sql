-- Verify saga-backend:create_auth_schema on pg

BEGIN;

  -- test that schema exists
  DO $$
    BEGIN
      ASSERT EXISTS(
        SELECT schema_name
        FROM information_schema.schemata
        WHERE schema_name = 'auth'
      );
    END;
  $$;

  -- test that api_anon can see it
  DO $$
    BEGIN
      ASSERT has_schema_privilege('api_anon', 'auth', 'usage');
      ASSERT NOT has_schema_privilege('api_anon', 'auth', 'create');
    END;
  $$;

ROLLBACK;
