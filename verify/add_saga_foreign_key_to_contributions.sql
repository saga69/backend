-- Verify saga-backend:add_saga_foreign_key_to_contributions on pg

BEGIN;



  -- insert into t_saga table to ensure saga exists
  INSERT INTO backend.t_sagas (title)
  VALUES ('Once upon a time');

  -- test basic insert functionality
  INSERT INTO backend.t_contributions (saga_id, parent_id, content)
  VALUES ((SELECT id FROM backend.t_sagas WHERE title = 'Once upon a time' LIMIT 1),
          NULL,
          'there was a lovely princess. But she had an enchantment upon her of a fearful sort which could only be broken by loves first kiss. She was locked away in a castle guarded by a terrible fire-breathing dragon. Many brave knights had attempted to free her from this dreadful prison, but non prevailed. She waited in the dragons keep in the highest room of the tallest tower for her true love and true loves first kiss.'),
        ((SELECT id FROM backend.t_sagas WHERE title = 'Once upon a time' LIMIT 1),
         NULL,
         'there was this dude. Not an ordinary dude, but extraordinary. No idea where this is going... but seems fun.');

  DO $$
    BEGIN
      ASSERT (SELECT COUNT(*) FROM backend.t_contributions) = 2;
    END;
  $$;

  -- test insert with parent_id that does not exist
  DO $$
    DECLARE
      message TEXT;
      saga_id INTEGER;
    BEGIN
      INSERT INTO backend.t_sagas (title)
      VALUES ('This old story') RETURNING id INTO saga_id;
      INSERT INTO backend.t_contributions (saga_id, parent_id, content)
      VALUES (saga_id, 0,'some text');
      RAISE EXCEPTION '%', 'this line shall not be reached';
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS message = MESSAGE_TEXT;
      ASSERT message = '{"errors": {"parent_id": "does not exist"}}';
    END;
  $$;

  -- test insert with null saga_id
  DO $$
    DECLARE
      message TEXT;
    BEGIN
      INSERT INTO backend.t_contributions (saga_id, parent_id, content)
      VALUES (NULL, 0,'some text');
      RAISE EXCEPTION '%', 'this line shall not be reached';
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS message = MESSAGE_TEXT;
      ASSERT message = '{"errors": {"saga_id": "shall not be empty"}}';
    END;
  $$;

  -- test insert into saga that does not contain the parent id
  DO $$
    DECLARE
      message TEXT;
      saga_id_main INTEGER;
      saga_id_new INTEGER;
      contribution_parent INTEGER;
    BEGIN
      INSERT INTO backend.t_sagas (title)
      VALUES ('New beginnings') RETURNING id INTO saga_id_main;
      INSERT INTO backend.t_sagas (title)
      VALUES ('Old things') RETURNING id INTO saga_id_new;
      INSERT INTO backend.t_contributions (saga_id, parent_id, content)
      VALUES (saga_id_main, NULL, 'old habits') RETURNING id INTO contribution_parent;
      INSERT INTO backend.t_contributions (saga_id, parent_id, content)
      VALUES (saga_id_new, contribution_parent, 'New thangs');
      RAISE EXCEPTION '%', 'this line should not be reached';
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS message = MESSAGE_TEXT;
      ASSERT message = '{"errors": {"parent_id": "does not exist"}}';
    END;
  $$;


ROLLBACK;
