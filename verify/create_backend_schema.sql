-- Verify saga-backend:create_backend_schema on pg

BEGIN;

  DO $$
    BEGIN
      ASSERT EXISTS(
        SELECT schema_name
        FROM information_schema.schemata
        WHERE schema_name = 'backend'
      );
    END;
  $$;

ROLLBACK;
