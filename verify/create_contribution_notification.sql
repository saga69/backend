-- Verify saga-backend:create_contribution_notification on pg

BEGIN;

  -- test that insert still works
  INSERT INTO backend.t_contributions (parent_id, content)
  VALUES (null, 'some content');

  -- test that api insert still works
  SET ROLE api_anon;
  INSERT INTO api.contributions (parent_id, content)
  VALUES (null, 'yo here');

ROLLBACK;
