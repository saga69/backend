-- Verify saga-backend:create_api_schema on pg

BEGIN;

  DO $$
    BEGIN
      ASSERT EXISTS(
        SELECT schema_name
        FROM information_schema.schemata
        WHERE schema_name = 'api'
      );
    END;
  $$;

ROLLBACK;
