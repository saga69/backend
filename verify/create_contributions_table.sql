-- Verify saga-backend:create_contributions_table on pg

BEGIN;

  -- test table presence
  SELECT * FROM backend.t_contributions;

  -- test basic insert functionality
  INSERT INTO backend.t_contributions (parent_id, content)
  VALUES (null, 'hello'), (null, 'hello'), (null, 'hello');

  DO $$
    BEGIN
      ASSERT (SELECT COUNT(*) FROM backend.t_contributions) = 3;
    END;
  $$;

  -- test that created_at will be set
  DO $$
    DECLARE
      before_time TIMESTAMP WITH TIME ZONE;
      after_time TIMESTAMP WITH TIME ZONE;
      real_time TIMESTAMP WITH TIME ZONE;
    BEGIN
      SELECT now() INTO before_time;
      INSERT INTO backend.t_contributions (parent_id, content)
      VALUES (null, 'time?') RETURNING created_at INTO real_time;
      SELECT now() INTO after_time;
      ASSERT before_time = real_time AND after_time = real_time;
    END;
  $$;


  -- test that created_at will be overriden
  DO $$
    DECLARE
      expected_time TIMESTAMP WITH TIME ZONE;
      real_time TIMESTAMP WITH TIME ZONE;
    BEGIN
      SELECT now() INTO expected_time;
      INSERT INTO backend.t_contributions (parent_id, content, created_at)
      VALUES (null, 'yo', now() + '1 day'::interval)
      RETURNING created_at INTO real_time;
      ASSERT real_time = expected_time;
    END;
  $$;

  -- test insert with parent_id that does not exist
  DO $$
    DECLARE
      message TEXT;
    BEGIN
      INSERT INTO backend.t_contributions (parent_id, content)
      VALUES (0,'some text');
      RAISE EXCEPTION '%', 'this line shall not be reached';
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS message = MESSAGE_TEXT;
      ASSERT message = '{"errors": {"parent_id": "does not exist"}}';
    END;
  $$;

  -- test insert with empty content
  DO $$
    DECLARE
      message TEXT;
    BEGIN
      INSERT INTO backend.t_contributions (content)
      VALUES ('');
      RAISE EXCEPTION '%', 'this line shall not be reached';
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS message = MESSAGE_TEXT;
      ASSERT message = '{"errors": {"content": "shall not be empty"}}';
    END;
  $$;

  -- test insert with null content
  DO $$
    DECLARE
      message TEXT;
    BEGIN
      INSERT INTO backend.t_contributions (content) VALUES (null);
      RAISE EXCEPTION '%', 'this line shall not be reached';
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS message = MESSAGE_TEXT;
      ASSERT message = '{"errors": {"content": "shall not be empty"}}';
    END;
  $$;

  -- test insert with bad parent and null content
  DO $$
    DECLARE
      message TEXT;
    BEGIN
      INSERT INTO backend.t_contributions (parent_id) VALUES (0);
      RAISE EXCEPTION '%', 'this line shall not be reached';
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS message = MESSAGE_TEXT;
      ASSERT message = '{"errors": {"content": "shall not be empty",'
                       ' "parent_id": "does not exist"}}';
    END;
  $$;

  -- test that repeating inserts with parents work
  DO $$
    DECLARE
      some_id INTEGER;
      before_count INTEGER;
      after_count INTEGER;
    BEGIN
      SELECT COUNT(*) FROM backend.t_contributions INTO before_count;

      INSERT INTO backend.t_contributions (parent_id, content)
      VALUES (null, 'some')
      RETURNING id INTO some_id;

      INSERT INTO backend.t_contributions (parent_id, content)
      VALUES (some_id, 'BODY')
           , (some_id + 1, 'once')
           , (some_id + 2, 'told')
           , (some_id + 3, 'me');

      SELECT COUNT(*) FROM backend.t_contributions INTO after_count;
      ASSERT before_count + 5 = after_count;
    END;
  $$;

  -- test that unicode symbols are allowed in content
  INSERT INTO backend.t_contributions (content) VALUES
    ('Жизнь — основное понятие биологии — активная форма существования материи, в некотором смысле высшая по сравнению с её физической и химической формами существования'),
    ('मुक्त ज्ञानकोश विकिपीडिया से'),
    ('El término vida (en latín: vita) desde la biología, hace referencia a aquello que distingue a los reinos animal, vegetal, hongos, protistas, arqueas y bacterias del resto de las realidades naturales'),
    ('生命泛指一类具有稳定的物质和能量代谢现象并且能回应刺激、能进行自我复制（繁殖）的半开放物质系统。');

ROLLBACK;
