-- Verify saga-backend:create_contributions_view on pg

BEGIN;

  -- check api_anon's privileges
  DO $$
    BEGIN
      ASSERT has_table_privilege('api_anon', 'api.contributions', 'select');
      ASSERT has_table_privilege('api_anon', 'api.contributions', 'insert');
      ASSERT NOT has_table_privilege('api_anon', 'api.contributions', 'update');
      ASSERT NOT has_table_privilege('api_anon', 'api.contributions', 'delete');
      ASSERT NOT has_table_privilege('api_anon', 'api.contributions', 'truncate');
      ASSERT NOT has_table_privilege('api_anon', 'api.contributions', 'references');
      ASSERT has_table_privilege('api_anon', 'api.contributions', 'trigger');
    END;
  $$;


  -- testing select
  INSERT INTO backend.t_contributions (parent_id, content) VALUES (null, 'text');

  SET ROLE api_anon;

  DO $$ BEGIN ASSERT (SELECT COUNT(*) FROM api.contributions) = 1; END; $$;

  -- testing insert
  INSERT INTO api.contributions (parent_id, content) VALUES (null, 'text');

  DO $$ BEGIN ASSERT (SELECT COUNT(*) FROM api.contributions) = 2; END; $$;

-- test insert of null content throws exception
  DO $$
    BEGIN
      INSERT INTO api.contributions (parent_id, content) VALUES (null, null);
    EXCEPTION WHEN OTHERS THEN
      ASSERT (sqlerrm LIKE '%shall not be empty%');
    END;
  $$;

  -- test insert of a parent id that doesn't exist
  DO $$
    BEGIN
      INSERT INTO api.contributions (parent_id, content) VALUES (0, 'test');
    EXCEPTION WHEN OTHERS THEN
      ASSERT (sqlerrm LIKE '%does not exist%');
    END;
  $$;

  -- test insert of a contribution with a parent id
  INSERT INTO api.contributions (parent_id, content) VALUES ((SELECT id FROM api.contributions limit 1), 'test');
  DO $$ BEGIN ASSERT (SELECT COUNT(*) FROM api.contributions) = 3; END; $$;

  -- test that updates are not possible
  DO $$
    BEGIN
      UPDATE api.contributions SET parent_id = null;
      RAISE EXCEPTION 'should not be reached!';
    EXCEPTION WHEN others THEN
      ASSERT (sqlerrm LIKE '%permission denied for view contributions%');
    END;
  $$;

  -- test that deletes are not possible
  DO $$
    BEGIN
      DELETE FROM api.contributions;
      RAISE EXCEPTION 'should not be reached!';
    EXCEPTION WHEN others THEN
      ASSERT (sqlerrm LIKE '%permission denied for view contributions%');
    END;
  $$;

ROLLBACK;
