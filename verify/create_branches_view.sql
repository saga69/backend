-- Verify saga-backend:create_branches_view on pg

BEGIN;

  CREATE TABLE test_branches (
    parent_tail           INT,
    head                  INT,
    tail                  INT,
    contribution_count    BIGINT,
    saga_id               INT,
    head_content          TEXT,
    contribution_ids      JSONB,
    contribution_contents JSONB
  );

  CREATE FUNCTION add_contribution(saga_id INT, parent_id INT, content TEXT)
  RETURNS INT
  LANGUAGE plpgsql AS $$
    DECLARE new_id INT;
    BEGIN
      INSERT INTO backend.t_contributions(saga_id, parent_id, content)
      VALUES (saga_id, parent_id, content) RETURNING id INTO new_id;
      RETURN new_id;
    END;
  $$;

  CREATE FUNCTION compare_branch_tables()
  RETURNS VOID
  LANGUAGE plpgsql AS $$
    DECLARE x RECORD;
    BEGIN
      IF (
        SELECT COUNT(*)
        FROM(
          (SELECT * FROM backend.v_branches EXCEPT SELECT * FROM test_branches)
          UNION ALL
          (SELECT * FROM test_branches EXCEPT SELECT * FROM backend.v_branches)
        ) compare
      ) <> 0 THEN
        RAISE NOTICE 'contents of backend.v_branches:';
        FOR x IN EXECUTE 'SELECT * FROM backend.v_branches' LOOP
          RAISE NOTICE '%', x;
        END LOOP;
        RAISE NOTICE 'expected contents:';
        FOR x IN EXECUTE 'SELECT * FROM test_branches' LOOP
          RAISE NOTICE '%', x;
        END LOOP;
        RAISE EXCEPTION 'branches list does not display proper branches!';
      END IF;
    END;
  $$;

  CREATE FUNCTION last_element(a INT[])
  RETURNS INT
  LANGUAGE plpgsql AS $$
    DECLARE e INT;
    BEGIN
      SELECT a[array_upper(a, 1)] INTO e;
      RETURN e;
    END;
  $$;

  DO $$
    DECLARE
      saga_id INT;
      contributions_ids INT[];
      tmp TEXT;
    BEGIN
      -- create saga
      INSERT INTO backend.t_sagas (title) VALUES ('test_me') RETURNING id INTO saga_id;

      -- add first contribution
      SELECT contributions_ids || add_contribution(saga_id, NULL, 'Hello, ')
      INTO contributions_ids;

      -- contribution graph
      -- @----1
      -- ^
      -- abstract NULL contribution

      -- add test branch
      INSERT INTO test_branches(
        parent_tail, head, tail,
        contribution_count, saga_id, head_content,
        contribution_ids, contribution_contents
      ) VALUES (
        NULL, last_element(contributions_ids), last_element(contributions_ids),
        1, saga_id, 'Hello, ',
        to_jsonb(contributions_ids),
        '["Hello, "]'::jsonb
      );

      -- check that branches are correct
      PERFORM compare_branch_tables();

      -- add another contribution following the last one
      SELECT contributions_ids || add_contribution(
        saga_id,
        last_element(contributions_ids),
        'astronaut accountants! '
      ) INTO contributions_ids;

      -- contribution graph
      -- @----1----2

      -- update test branches
      UPDATE test_branches
         SET tail = last_element(contributions_ids)
           , contribution_count = 2
           , contribution_ids = to_jsonb(contributions_ids)
           , contribution_contents = '["Hello, ", "astronaut accountants! "]'::jsonb;

      -- check that branches are correct
      PERFORM compare_branch_tables();

      -- add 5 more consecutive contributions
      FOREACH tmp IN ARRAY '{
        Ace and Jocelyn here,
        reporting live from Ace''s closet.,
        Are you ready?,
        I am not.,
        Let''s hit it Ace.
      }'::TEXT[] LOOP
        SELECT contributions_ids || add_contribution(
          saga_id,
          last_element(contributions_ids),
          tmp || ' '
        ) INTO contributions_ids;
      END LOOP;

      -- contribution graph
      -- @----1----2----3----4----5----6----7

      -- update test branches
      UPDATE test_branches SET
        tail = last_element(contributions_ids),
        contribution_count = 7,
        contribution_ids = jsonb_build_array(
          contributions_ids[1],
          contributions_ids[2],
          contributions_ids[3],
          contributions_ids[4],
          contributions_ids[5],
          contributions_ids[6],
          contributions_ids[7]
        ),
        contribution_contents = '[
          "Hello, ",
          "astronaut accountants! ",
          "Ace and Jocelyn here ",
          "reporting live from Ace''s closet. ",
          "Are you ready? ",
          "I am not. ",
          "Let''s hit it Ace. "
        ]'::jsonb;

      -- check that branches are correct
      PERFORM compare_branch_tables();

      -- create a contribution that creates a "branch"
      SELECT contributions_ids || add_contribution(
        saga_id, contributions_ids[3], 'Jocelyn here,'
      ) INTO contributions_ids;

      -- contribution graph
      -- @----1----2----3----4----5----6----7
      --                 \
      --                  8

      -- update/add test branches
      UPDATE test_branches SET
        tail = contributions_ids[3],
        contribution_count = 3,
        contribution_ids = jsonb_build_array(
          contributions_ids[1],
          contributions_ids[2],
          contributions_ids[3]
        ),
        contribution_contents = '[
          "Hello, ",
          "astronaut accountants! ",
          "Ace and Jocelyn here "
        ]'::jsonb;

      INSERT INTO test_branches (
        parent_tail, head, tail,
        contribution_count, saga_id, head_content,
        contribution_ids, contribution_contents
      ) VALUES
        (
          contributions_ids[3], contributions_ids[4], contributions_ids[7],
          4, saga_id, 'reporting live from Ace''s closet. ',
          jsonb_build_array(
            contributions_ids[4],
            contributions_ids[5],
            contributions_ids[6],
            contributions_ids[7]
          ),
          '[
            "reporting live from Ace''s closet. ",
            "Are you ready? ",
            "I am not. ",
            "Let''s hit it Ace. "
          ]'::jsonb
        ),
        (
          contributions_ids[3],
          last_element(contributions_ids), last_element(contributions_ids),
          1, saga_id, 'Jocelyn here,',
          jsonb_build_array(last_element(contributions_ids)),
          '["Jocelyn here,"]'::jsonb
        );

      -- check that branches are correct;
      PERFORM compare_branch_tables();

      -- continue building the new "branch"
      SELECT contributions_ids || add_contribution(
        saga_id, last_element(contributions_ids), ' and today we are going to'
      ) INTO contributions_ids;
      SELECT contributions_ids || add_contribution(
        saga_id, last_element(contributions_ids), ' stop making stupid jokes.'
      ) INTO contributions_ids;

      -- contribution graph
      -- @----1----2----3----4----5----6----7
      --                 \
      --                  8----9---10

      -- update branch
      UPDATE test_branches
         SET tail = last_element(contributions_ids)
           , contribution_count = 3
           , contribution_ids = jsonb_build_array(
               contributions_ids[8],
               contributions_ids[9],
               contributions_ids[10]
             )
           , contribution_contents = '[
               "Jocelyn here,",
               " and today we are going to",
               " stop making stupid jokes."
             ]'::jsonb
       WHERE contribution_count = 1;

      -- check that branches are correct;
      PERFORM compare_branch_tables();

      -- create a contribution out of id sequence (in the original branch)
      SELECT contributions_ids || add_contribution(
        saga_id, (
            SELECT tail
              FROM backend.v_branches
          ORDER BY contribution_count DESC
             LIMIT 1
        ), ' Thank for reading.'
      ) INTO contributions_ids;

      -- contribution graph
      -- @----1----2----3----4----5----6----7---11
      --                 \
      --                  8----9---10

      -- updating test branches
      UPDATE test_branches
         SET tail = last_element(contributions_ids)
           , contribution_count = contribution_count + 1
           , contribution_ids = contribution_ids || jsonb_build_array(
               last_element(contributions_ids)
             )
           , contribution_contents = contribution_contents || '[" Thank for reading."]'::jsonb
       WHERE contribution_count = 4;

      -- check that branches are correct;
      PERFORM compare_branch_tables();

      -- branch off of a branch
      SELECT contributions_ids || add_contribution(
        saga_id, contributions_ids[9], ' No-no-no, thank you!'
      ) INTO contributions_ids;

      -- contribution graph
      -- @----1----2----3----4----5----6----7---11
      --                 \
      --                  8----9---10
      --                        \
      --                        12

      -- update/add test branches
      UPDATE test_branches
         SET tail = contributions_ids[9]
           , contribution_count = 2
           , contribution_ids = jsonb_build_array(
               contributions_ids[8],
               contributions_ids[9]
             )
           , contribution_contents = '[
               "Jocelyn here,",
               " and today we are going to"
             ]'::jsonb
       WHERE tail = contributions_ids[10];

      INSERT INTO test_branches (
        parent_tail, head, tail,
        contribution_count, saga_id, head_content,
        contribution_ids, contribution_contents
      ) VALUES
        (
          contributions_ids[9], contributions_ids[10], contributions_ids[10],
          1, saga_id, ' stop making stupid jokes.',
          jsonb_build_array(contributions_ids[10]),
          '[" stop making stupid jokes."]'::jsonb
        ),
        (
          contributions_ids[9], contributions_ids[12], contributions_ids[12],
          1, saga_id, ' No-no-no, thank you!',
          jsonb_build_array(contributions_ids[12]),
          '[" No-no-no, thank you!"]'::jsonb
        );

      -- check that branches are correct;
      PERFORM compare_branch_tables();

      -- branch off again at the first branching point
      SELECT contributions_ids || add_contribution(
        saga_id, contributions_ids[3], ' whatever.'
      ) INTO contributions_ids;

      -- contribution graph
      -- @----1----2----3----4----5----6----7---11
      --                |\
      --                | 8----9---10
      --                |       \
      --                |       12
      --                \
      --                13

      -- add test branches
      INSERT INTO test_branches (
        parent_tail, head, tail, contribution_count,
        saga_id, head_content, contribution_ids, contribution_contents
      ) VALUES (
        contributions_ids[3], contributions_ids[13], contributions_ids[13],
        1, saga_id, ' whatever.',
        jsonb_build_array(contributions_ids[13]),
        '[" whatever."]'::jsonb
      );

      -- check that branches are correct;
      PERFORM compare_branch_tables();

      -- add another saga with some contributions
      INSERT INTO backend.t_sagas (title) VALUES ('Another one?')
      RETURNING id INTO saga_id;

      SELECT contributions_ids || add_contribution(
        saga_id, NULL, 'Hi. '
      ) INTO contributions_ids;
      SELECT contributions_ids || add_contribution(
        saga_id, last_element(contributions_ids), 'The end.'
      ) INTO contributions_ids;

      -- update test branches
      INSERT INTO test_branches (
        parent_tail, head, tail,
        contribution_count, saga_id, head_content,
        contribution_ids, contribution_contents
      ) VALUES (
        NULL, contributions_ids[14], last_element(contributions_ids),
        2, saga_id, 'Hi. ',
        jsonb_build_array(contributions_ids[14], contributions_ids[15]),
        '["Hi. ", "The end."]'::jsonb
      );

      -- check that branches are correct;
      PERFORM compare_branch_tables();

    END;
  $$;

ROLLBACK;
