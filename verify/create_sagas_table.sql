-- Verify saga-backend:create_sagas_table on pg

BEGIN;

  -- test table presence
  SELECT * FROM backend.t_sagas;

  -- test basic functionality
  INSERT INTO backend.t_sagas (title)
  VALUES ('My title'), ('Once upon a time...'), ('Something Original');

  DO $$
    BEGIN
      ASSERT (SELECT COUNT(*) from backend.t_sagas) = 3;
    END;
  $$;

  -- test that created_at will be set
  DO $$
    DECLARE
      before_time TIMESTAMP WITH TIME ZONE;
      after_time TIMESTAMP WITH TIME ZONE;
      real_time TIMESTAMP WITH TIME ZONE;
    BEGIN
      SELECT now() INTO before_time;
      INSERT INTO backend.t_sagas (title)
      VALUES ('My Creative Title') RETURNING created_at INTO real_time;
      SELECT now() INTO after_time;
      ASSERT before_time = real_time AND after_time = real_time;
    END;
  $$;

  -- test that created_at will be overriden
  DO $$
    DECLARE
      expected_time TIMESTAMP WITH TIME ZONE;
      real_time TIMESTAMP WITH TIME ZONE;
    BEGIN
      SELECT now() INTO expected_time;
      INSERT INTO backend.t_sagas (title, created_at)
      VALUES ('Boi, this story really happened', now() + '2 day'::interval)
      RETURNING created_at INTO real_time;
      ASSERT real_time = expected_time;
    END;
  $$;

  -- test insert with empty title
  DO $$
    DECLARE
      message TEXT;
    BEGIN
      INSERT INTO backend.t_sagas (title)
      VALUES ('');
      RAISE EXCEPTION '%', 'this line should not be reached';
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS message = MESSAGE_TEXT;
      ASSERT message = '{"errors": {"title": "shall not be empty"}}';
    END;
  $$;

  -- test insert with null content
  DO $$
    DECLARE
      message TEXT;
    BEGIN
      INSERT INTO backend.t_sagas (title)
      VALUES (NULL);
      RAISE EXCEPTION '%', 'this line should not be reached';
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS message = MESSAGE_TEXT;
      ASSERT message = '{"errors": {"title": "shall not be empty"}}';
    END;
  $$;

  -- test that unicode symvols are allowed for title
  INSERT INTO backend.t_sagas (title)
  VALUES ('คนที่อ้วนเร็ว'), ('لها يا مجلس الاستثمار'), ('בדיוק קיבלתי את השעון הזה מאבא שלי לפני שהוא נפטר');

ROLLBACK;
