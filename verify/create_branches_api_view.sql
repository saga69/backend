-- Verify saga-backend:create_branches_api_view on pg

BEGIN;

  -- check that view is selectable from
  SELECT * FROM api.branches;

  -- check that view is selectable from by api_anon;
  SET ROLE api_anon;

  SELECT * FROM api.branches;

  RESET ROLE;

  -- check that api_anon is only allowed to select from view
  DO $$
    BEGIN
      ASSERT has_table_privilege('api_anon', 'api.branches', 'select');
      ASSERT NOT has_table_privilege('api_anon', 'api.branches', 'insert');
      ASSERT NOT has_table_privilege('api_anon', 'api.branches', 'update');
      ASSERT NOT has_table_privilege('api_anon', 'api.branches', 'delete');
      ASSERT NOT has_table_privilege('api_anon', 'api.branches', 'truncate');
      ASSERT NOT has_table_privilege('api_anon', 'api.branches', 'references');
      ASSERT NOT has_table_privilege('api_anon', 'api.branches', 'trigger');
    END;
  $$;

  -- add some sagas and contributions
  CREATE TABLE my_test_branches (
    parent_tail           INT,
    head                  INT,
    tail                  INT,
    contribution_count    BIGINT,
    saga_id               INT,
    head_content          TEXT,
    contribution_ids      JSONB,
    contribution_contents JSONB
  );

    WITH saga_ids AS (
           INSERT INTO backend.t_sagas (title) VALUES ('saga #1'), ('saga #2')
           RETURNING id
         )
       , root_contribution_id AS (
           INSERT INTO backend.t_contributions (saga_id, parent_id, content)
           VALUES (
             (SELECT id FROM saga_ids ORDER BY id ASC LIMIT 1), NULL, 'Root contribution'
           ) RETURNING id
         )
       , second_contribution_id AS (
           INSERT INTO backend.t_contributions (saga_id, parent_id, content)
           VALUES (
             (SELECT id FROM saga_ids ORDER BY id ASC LIMIT 1),
             (SELECT id FROM root_contribution_id),
             'Regular contribution'
           ) RETURNING id
         )
       , third_contribution_id AS (
           INSERT INTO backend.t_contributions (saga_id, parent_id, content)
           VALUES (
             (SELECT id FROM saga_ids ORDER BY id ASC LIMIT 1),
             (SELECT id FROM second_contribution_id),
             'Another regular contribution'
           ) RETURNING id
         )
       , branching_contribution_id AS (
           INSERT INTO backend.t_contributions (saga_id, parent_id, content)
           VALUES (
             (SELECT id FROM saga_ids ORDER BY id ASC LIMIT 1),
             (SELECT id FROM second_contribution_id),
             'This contribution starts a branch'
           ) RETURNING id
         )
       , root_contribution_in_another_saga_id AS (
           INSERT INTO backend.t_contributions (saga_id, parent_id, content)
           VALUES (
             (SELECT id FROM saga_ids ORDER BY id DESC LIMIT 1),
             NULL, 'Another saga''s root contribution'
           ) RETURNING id
         )
       , root_branch_id_in_first_saga AS (
           INSERT INTO my_test_branches (
             parent_tail, head, tail,
             contribution_count, head_content, saga_id,
             contribution_ids, contribution_contents
           ) VALUES (
             NULL,
             (SELECT id FROM root_contribution_id),
             (SELECT id FROM second_contribution_id),
             2, 'Root contribution',
             (SELECT id FROM saga_ids ORDER BY id ASC LIMIT 1),
             jsonb_build_array(
               (SELECT id FROM root_contribution_id),
               (SELECT id FROM second_contribution_id)
             ),
             '["Root contribution", "Regular contribution"]'::jsonb
           ) RETURNING tail
         )
       , second_branch_id_in_first_saga AS (
           INSERT INTO my_test_branches (
             parent_tail, head, tail,
             contribution_count, head_content, saga_id,
             contribution_ids, contribution_contents
           ) VALUES (
             (SELECT tail FROM root_branch_id_in_first_saga),
             (SELECT id FROM third_contribution_id),
             (SELECT id FROM third_contribution_id),
             1, 'Another regular contribution',
             (SELECT id FROM saga_ids ORDER BY id ASC LIMIT 1),
             jsonb_build_array((SELECT id FROM third_contribution_id)),
             '["Another regular contribution"]'::jsonb
           ) RETURNING tail
         )
       , third_branch_id_in_first_saga AS (
           INSERT INTO my_test_branches (
             parent_tail, head, tail,
             contribution_count, head_content, saga_id,
             contribution_ids, contribution_contents
           ) VALUES (
             (SELECT tail FROM root_branch_id_in_first_saga),
             (SELECT id FROM branching_contribution_id),
             (SELECT id FROM branching_contribution_id),
             1, 'This contribution starts a branch',
             (SELECT id FROM saga_ids ORDER BY id ASC LIMIT 1),
             jsonb_build_array((SELECT id FROM branching_contribution_id)),
             '["This contribution starts a branch"]'::jsonb
           ) RETURNING tail
         )
       , the_only_branch_in_second_saga AS (
           INSERT INTO my_test_branches (
             parent_tail, head, tail,
             contribution_count, head_content, saga_id,
             contribution_ids, contribution_contents
           ) VALUES (
             NULL,
             (SELECT id FROM root_contribution_in_another_saga_id),
             (SELECT id FROM root_contribution_in_another_saga_id),
             1, 'Another saga''s root contribution',
             (SELECT id FROM saga_ids ORDER BY id DESC LIMIT 1),
             jsonb_build_array((SELECT id FROM root_contribution_in_another_saga_id)),
             '["Another saga''s root contribution"]'::jsonb
           ) RETURNING tail
         )
  -- contributions & sagas
  SELECT * FROM third_contribution_id
   UNION ALL
  SELECT * FROM branching_contribution_id
   UNION ALL
  SELECT * FROM root_contribution_in_another_saga_id
  -- test brnaches
   UNION ALL
  SELECT * FROM second_branch_id_in_first_saga
   UNION ALL
  SELECT * FROM third_branch_id_in_first_saga
   UNION ALL
  SELECT * FROM the_only_branch_in_second_saga;

  -- compare results in api.branches and my_test_branches from api_anon's perspective
  GRANT SELECT ON my_test_branches TO api_anon;
  SET ROLE api_anon;

  DO $$
    DECLARE x RECORD;
    BEGIN
      IF (
           SELECT COUNT (*) FROM (
             (SELECT * FROM api.branches EXCEPT SELECT * FROM my_test_branches)
             UNION ALL
             (SELECT * FROM my_test_branches EXCEPT SELECT * FROM api.branches)
           ) AS res
         ) <> 0 THEN
        RAISE NOTICE 'contents of api.branches:';
        FOR x IN EXECUTE 'SELECT * FROM api.branches' LOOP
          RAISE NOTICE '%', x;
        END LOOP;
        RAISE NOTICE 'expected contents:';
        FOR x IN EXECUTE 'SELECT * FROM my_test_branches' LOOP
          RAISE NOTICE '%', x;
        END LOOP;
        RAISE EXCEPTION 'branches list does not display proper branches!';
      END IF;
    END
  $$;

  RESET ROLE;

ROLLBACK;
