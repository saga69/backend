-- Verify saga-backend:modify_contributions_validation_function on pg

BEGIN;

  -- test insert valid root saga contribution
  DO $$
    DECLARE
      saga_id INTEGER;
      contribution_id INTEGER;
    BEGIN
      INSERT INTO backend.t_sagas (title)
      VALUES ('Join tables') RETURNING id INTO saga_id;
      INSERT INTO backend.t_contributions (saga_id, parent_id, content)
      VALUES (saga_id, NULL, 'are great');
    END;
  $$;


  -- test insert invalid root saga contribution
  DO $$
    DECLARE
      message TEXT;
      saga_id INTEGER;
      contribution_id INTEGER;
    BEGIN
      INSERT INTO backend.t_sagas (title)
      VALUES ('The Title') RETURNING id INTO saga_id;
      INSERT INTO backend.t_contributions (saga_id, parent_id, content)
      VALUES (saga_id, NULL, 'Old House');
      INSERT INTO backend.t_contributions (saga_id, parent_id, content)
      VALUES (saga_id, NULL, 'New Place');
      RAISE EXCEPTION '%', 'this line should not be reached';
    EXCEPTION WHEN OTHERS THEN
      GET STACKED DIAGNOSTICS message = MESSAGE_TEXT;
      ASSERT message = '{"errors": {"parent_id": "has multiple root contributions"}}';
    END;
  $$;

ROLLBACK;
