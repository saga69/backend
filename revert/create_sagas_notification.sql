-- Revert saga-backend:create_sagas_notification from pg

BEGIN;


  DROP TRIGGER tg_contributions_4 ON backend.t_contributions;
  DROP FUNCTION backend.fn_sagas_notification();

COMMIT;
