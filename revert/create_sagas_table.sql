-- Revert saga-backend:create_sagas_table from pg

BEGIN;

  DROP TRIGGER tg_sagas_2 ON backend.t_sagas;
  DROP TRIGGER tg_sagas_1 ON backend.t_sagas;

  DROP FUNCTION backend.fn_sagas_validation;
  DROP FUNCTION backend.fn_sagas_pre_insert;

  DROP TABLE backend.t_sagas;

COMMIT;
