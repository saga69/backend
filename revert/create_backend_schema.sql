-- Revert saga-backend:create_backend_schema from pg

BEGIN;

  DROP SCHEMA backend;

COMMIT;
