-- Revert saga-backend:create_branches_view from pg

BEGIN;

  DROP VIEW backend.v_branches;

COMMIT;
