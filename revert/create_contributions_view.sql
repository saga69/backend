-- Revert saga-backend:create_contributions_view from pg

BEGIN;

  REVOKE SELECT ON api.contributions FROM api_anon;
  REVOKE INSERT ON api.contributions FROM api_anon;
  REVOKE TRIGGER ON api.contributions FROM api_anon;
  REVOKE USAGE, SELECT ON SEQUENCE backend.t_contributions_id_seq FROM api_anon;

  DROP VIEW api.contributions;

COMMIT;
