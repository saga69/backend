-- Revert saga-backend:create_contributions_table from pg

BEGIN;

  DROP TRIGGER tg_contributions_2 ON backend.t_contributions;
  DROP TRIGGER tg_contributions_1 ON backend.t_contributions;

  DROP FUNCTION backend.fn_contributions_validation;
  DROP FUNCTION backend.fn_contributions_pre_insert;

  DROP TABLE backend.t_contributions;

COMMIT;
