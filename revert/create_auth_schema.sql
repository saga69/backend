-- Revert saga-backend:create_auth_schema from pg

BEGIN;

  REVOKE USAGE ON SCHEMA auth FROM api_anon;

  DROP SCHEMA auth;

COMMIT;
