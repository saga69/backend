-- Revert saga-backend:create_sagas_view from pg

BEGIN;

  REVOKE SELECT ON api.sagas FROM api_anon;
  REVOKE INSERT ON api.sagas FROM api_anon;
  REVOKE TRIGGER ON api.sagas FROM api_anon;
  REVOKE USAGE, SELECT  ON backend.t_sagas_id_seq FROM api_anon;
  REVOKE SELECT ON backend.t_sagas FROM api_anon;

  DROP VIEW api.sagas;

COMMIT;
