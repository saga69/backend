-- Revert saga-backend:create_contribution_notification from pg

BEGIN;

  DROP TRIGGER tg_contributions_3 ON backend.t_contributions;
  DROP FUNCTION backend.fn_contributions_notification();

COMMIT;
