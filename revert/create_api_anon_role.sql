-- Revert saga-backend:create_api_anon_role from pg

BEGIN;

  REVOKE USAGE ON SCHEMA api FROM api_anon;

  DROP ROLE api_anon;

COMMIT;
