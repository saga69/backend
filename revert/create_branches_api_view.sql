-- Revert saga-backend:create_branches_api_view from pg

BEGIN;

  REVOKE SELECT ON api.branches FROM api_anon;

  DROP VIEW api.branches;

COMMIT;
