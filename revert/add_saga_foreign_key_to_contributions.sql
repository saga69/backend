-- Revert saga-backend:add_saga_foreign_key_to_contributions from pg

BEGIN;

  -- Sorry for this ugly code.
  -- Postgresql does NOT allow to remove columns
  -- from a view, you are only able to add.
  -- We have to drop and recreate the view to get this
  -- to work.
  REVOKE SELECT ON api.contributions FROM api_anon;
  REVOKE INSERT ON api.contributions FROM api_anon;
  REVOKE TRIGGER ON api.contributions FROM api_anon;
  REVOKE USAGE, SELECT ON SEQUENCE backend.t_contributions_id_seq FROM api_anon;

  DROP VIEW api.contributions;

  ALTER TABLE backend.t_contributions
  DROP CONSTRAINT fk_saga_id;

  ALTER TABLE backend.t_contributions
  DROP column saga_id;

  DROP FUNCTION backend.fn_first_t_saga;

  -- Contirbutions view acts as a facade around backend.t_contributions
  CREATE VIEW api.contributions AS
    SELECT * FROM backend.t_contributions;

  GRANT SELECT ON api.contributions TO api_anon;
  GRANT INSERT ON api.contributions TO api_anon;
  GRANT TRIGGER ON api.contributions TO api_anon;
  GRANT USAGE, SELECT ON SEQUENCE backend.t_contributions_id_seq TO api_anon;

COMMIT;

