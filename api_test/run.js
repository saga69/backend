var glob = require('glob');

glob("./@(unit|integration)/**/*.js", {}, async function (er, files) {
  funcs = []
  files.forEach(f => funcs.push(...require(f)));
  for (func of funcs) {
    try {
      console.log(`> starting ${func.name}`);
      await func();
      console.log(`> finished ${func.name}`);
    } catch (e) {
      console.log('ERROR ENCOUNTERED');
      console.error(e);
      process.exit(1);
    }
  }
});
