// useful libraries
request = require('async-request');
assert = require('assert');
var ws = require('websocket').w3cwebsocket;
const { Pool } = require('pg');
const path = require('path');
const dotenv = require('dotenv').config({ path: path.resolve(__dirname, '../.env') });

// constants
global.ws_host = process.env.API_TEST_WS_HOST || 'localhost';
global.api_host = process.env.API_TEST_API_HOST || 'localhost';
global.db_host = process.env.API_TEST_DB_HOST || 'localhost';

global.api_base_url = `http://${api_host}:3000`;
global.ws_base_url = `ws://${ws_host}:3001`;


var pg_query = async function(q) {
  var pg = new Pool({
    user: 'saga_user',
    host: db_host,
    password: process.env.SECRET_POSTGRES_PASSWORD,
    database: 'saga_db',
    port: 5432
  });
  var res = await pg.query(q);
  pg.end();
  return res;
}

module.exports = [
  request,
  assert,
  query = pg_query,
  WebSocket = ws,
];
