require ('../../test_helper.js')

module.exports = [
  async function test_that_contirbutions_can_be_retrieved() {
    let res = await request(
      `${api_base_url}/contributions`,
      { method: 'GET' }
    );

    assert(res.statusCode === 200);
    assert(!JSON.parse(res.body).length);
  },
  async function test_that_contributions_can_be_retrieved_with_some_data() {
    let saga = await query(`
      INSERT INTO backend.t_sagas(title)
      VALUES ('my saga') RETURNING id
    `);
    let saga_id = saga.rows[0].id;

    let res = await query(`
      INSERT INTO backend.t_contributions (saga_id, parent_id,content)
      VALUES (${saga_id}, null, 'hi!!!') RETURNING id
    `);
    let new_id = res.rows[0].id;

    res = await request(
      `${api_base_url}/contributions`,
      { method: 'GET' }
    );

    assert.equal(res.statusCode, 200);
    assert.equal(JSON.parse(res.body).length, 1);
    assert.equal(JSON.parse(res.body)[0].id, new_id);
    assert.equal(JSON.parse(res.body)[0].content, 'hi!!!');

    await query(`DELETE FROM backend.t_contributions WHERE id = ${new_id}`);
    await query(`DELETE FROM backend.t_sagas WHERE id = ${saga_id}`);
  }
];
