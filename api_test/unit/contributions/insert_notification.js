require ('../../test_helper.js')
var fs = require('fs');

module.exports = [
  async function test_that_websocket_get_insert_notifications() {
    let token = fs.readFileSync('../jwt', 'utf8');
    var ws = new WebSocket(`${ws_base_url}/contributions/${token}`);
    ws.onerror = function(e) {
      console.log(e);
    };
    ws.onopen = function(conn) {
      assert(true);
    };
    ws.onmessage = function(message) {
      assert(JSON.parse(message.data).payload === 'new contribution');
      assert(JSON.parse(message.data).channel === 'contributions');
    };

    await query(`
      DO $$
      DECLARE
        new_id INT;
        new_cont_id INT;
      BEGIN
        INSERT INTO backend.t_sagas (title) VALUES ('new sage!')
        RETURNING id INTO new_id;
        INSERT INTO backend.t_contributions(saga_id, parent_id, content)
        VALUES (new_id, null, 'notify about me!')
        RETURNING id INTO new_cont_id;

        DELETE FROM backend.t_contributions WHERE id = new_cont_id;
        DELETE FROM backend.t_sagas WHERE id = new_id;
      END;
      $$;
    `);
    ws.close();

  },
  async function test_that_websocket_get_insert_notifications_for_saga() {
    fs.readFile('../jwt', 'utf8', async function(_, token) {
      let saga_query_result = await query(`
        INSERT INTO backend.t_sagas(title)
        VALUES ('Notify to this group!') RETURNING id
      `);
      let saga_id = saga_query_result.rows[0].id;
      var ws = new WebSocket(`${ws_base_url}/saga-${saga_id}/${token}`);
      ws.onerror = function(e) {
        console.log(e);
      };
      ws.onopen = function(conn) {
        assert(true);
      };
      ws.onmessage = function(message) {
        assert(JSON.parse(message.data).payload === 'new notification');
        assert(JSON.parse(message.data).channel === 'sagas-' || saga_id);
      };

      let query_result = await query(`
        INSERT INTO backend.t_contributions(saga_id, parent_id, content)
        VALUES (${saga_id}, null, 'notify about me!') RETURNING id
      `);
      await query(`
        DELETE FROM backend.t_contributions WHERE id = ${query_result.rows[0].id}
      `);
      await query(`
        DELETE FROM backend.t_sagas WHERE id = ${saga_id}
      `);

      ws.close();
    });
  }

]
