require ('../../test_helper.js')

var contribution_count = async function() {
  return (await query(`SELECT COUNT(*) AS count FROM backend.t_contributions`)).rows[0].count;
}

module.exports = [
  async function test_that_contributions_can_be_created() {
    let saga_id = await query(`
      INSERT INTO backend.t_sagas (title) VALUES ('hey')
      RETURNING id
    `);
    saga_id = saga_id.rows[0].id;
    let res = await request(
      `${api_base_url}/contributions`,
      {
        method: 'POST',
        data: { content: 'notice me!', saga_id: saga_id }
      }
    );

    assert.equal(res.statusCode, 201);
    let id = res.headers.location.match(/.id=eq\.(\d+)/)[1];
    res = await query(`
      SELECT * FROM backend.t_contributions WHERE id = ${id}
    `);
    assert.equal(res.rows.length, 1);
    assert.equal(res.rows[0].id, id);
    assert.equal(res.rows[0].content, 'notice me!');
    await query(`
      DELETE FROM backend.t_contributions WHERE id = ${res.rows[0].id};
      DELETE FROM backend.t_sagas WHERE id = ${saga_id}
    `);
  },
  async function test_that_contribution_cannot_be_posted_without_content() {
    let count_before = await contribution_count();
    let saga_id = await query(`
      INSERT INTO backend.t_sagas (title) VALUES ('hey')
      RETURNING id
    `);
    saga_id = saga_id.rows[0].id;
    let res = await request(
      `${api_base_url}/contributions`,
      {
        method: 'POST',
        data: { content: null, saga_id: saga_id }
      }
    );
    assert.equal(res.statusCode, 400);
    assert.deepEqual(
      JSON.parse(
        JSON.parse(res.body).message
      ).errors,
      { content: 'shall not be empty' }
    );
    let count_after = await contribution_count();
    assert.equal(count_after, count_before);
    await query(`DELETE FROM backend.t_sagas WHERE id = ${saga_id}`);
  },
  async function test_that_contribution_cannot_be_posted_with_bad_parent() {
    let count_before = await contribution_count();
    let saga_id = await query(`
      INSERT INTO backend.t_sagas (title) VALUES ('hey')
      RETURNING id
    `);
    saga_id = saga_id.rows[0].id;

    let res = await request(
      `${api_base_url}/contributions`,
      {
        method: 'POST',
        data: {
          parent_id: 0,
          content: 'hello, I am ',
          saga_id: saga_id
        }
      }
    );
    assert.equal(res.statusCode, 400);
    assert.deepEqual(
      JSON.parse(
        JSON.parse(res.body).message
      ).errors,
      { parent_id: 'does not exist' }
    );
    let count_after = await contribution_count();
    assert.equal(count_before, count_after);
    await query(`DELETE FROM backend.t_sagas WHERE id = ${saga_id}`);
  },
  async function test_that_contribution_cannot_be_posted_with_invalid_parent_id() {
    let count_before = await contribution_count();

    let saga_id = await query(`
      INSERT INTO backend.t_sagas (title) VALUES ('hey')
      RETURNING id
    `);
    saga_id = saga_id.rows[0].id;
    let res = await request(
      `${api_base_url}/contributions`,
      {
        method: 'POST',
        data: {
          parent_id: 'hello',
          content: 'good bye',
          saga_id: saga_id
        }
      }
    );
    assert.equal(res.statusCode, 400);
    assert.equal(
      JSON.parse(res.body).message,
      'invalid input syntax for type integer: "hello"'
    );

    let count_after = await contribution_count();
    assert.equal(count_before, count_after);
    await query(`DELETE FROM backend.t_sagas WHERE id = ${saga_id}`);
  },
  async function test_that_contribution_cannot_be_posted_with_bad_parent_and_empty_content() {
    let count_before = await contribution_count();

    let saga_id = await query(`
      INSERT INTO backend.t_sagas (title) VALUES ('hey')
      RETURNING id
    `);
    saga_id = saga_id.rows[0].id;
    let res = await request(
      `${api_base_url}/contributions`,
      {
        method: 'POST',
        data: {
          parent_id: 0,
          saga_id: saga_id
        }
      }
    );
    assert.equal(res.statusCode, 400);
    assert.deepEqual(
      JSON.parse(
        JSON.parse(res.body).message
      ).errors,
      {
        parent_id: 'does not exist',
        content: 'shall not be empty'
      }
    );

    let count_after = await contribution_count();
    assert.equal(count_before, count_after);
    await query(`DELETE FROM backend.t_sagas WHERE id = ${saga_id}`);
  },
  async function test_that_multiple_contributions_can_be_inserted() {

    let saga_id = await query(`
      INSERT INTO backend.t_sagas (title) VALUES ('hey')
      RETURNING id
    `);
    saga_id = saga_id.rows[0].id;
    let contribution_parent_id = await query(`
      INSERT INTO backend.t_contributions (saga_id, parent_id, content)
      VALUES (${saga_id}, null, 'base')
      RETURNING id
    `);
    contribution_parent_id = contribution_parent_id.rows[0].id;
    let count_before = await contribution_count();
    for (let i = 0; i < 10; i++) {
      let res = await request(
        `${api_base_url}/contributions`,
        {
          method: 'POST',
          data: {
            content: '<>',
            saga_id: saga_id,
            parent_id: contribution_parent_id
          }
        }
      );
      assert.equal(res.statusCode, 201);
    }

    let count_after = await contribution_count();
    assert.equal(count_before, count_after - 10);

    await query(`
      DELETE FROM backend.t_contributions
      WHERE saga_id = ${saga_id}
    `);
    await query(`DELETE FROM backend.t_sagas WHERE id = ${saga_id}`);
  },
  async function test_that_multiple_root_contributions_cannot_be_inserted() {
    let saga_id = await query(`
      INSERT INTO backend.t_sagas (title) VALUES ('Two roots one saga')
      RETURNING id
    `);
    saga_id = saga_id.rows[0].id;
    let res = await request(
      `${api_base_url}/contributions`,
      {
        method: 'POST',
        data: {
          content: 'Root #1',
          saga_id: saga_id
        }
      }
    );
    assert.equal(res.statusCode, 201);
    res = await request(
      `${api_base_url}/contributions`,
      {
        method: 'POST',
        data: {
          content: 'Root #2',
          saga_id: saga_id
        }
      }
    );
    assert.equal(res.statusCode, 400);
    assert.deepEqual(
      JSON.parse(
        JSON.parse(res.body).message
      ),
      { errors: { parent_id: 'has multiple root contributions' } }
    );
    await query(`
      DELETE FROM backend.t_contributions WHERE saga_id = ${saga_id};
      DELETE FROM backend.t_sagas WHERE id = ${saga_id}
    `);
  }
]
