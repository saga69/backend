require ('../../test_helper.js')

module.exports = [
  async function test_that_branches_route_is_accessible() {
    let res = await request(
      `${api_base_url}/branches`,
      { method: 'GET' }
    );

    assert.equal(res.statusCode, 200);
    assert(!JSON.parse(res.body).length);
  },
  async function test_that_branches_are_retrievable_with_some_data() {
    let db_data = await query(`
      WITH saga AS ( INSERT INTO backend.t_sagas (title) VALUES ('testing?') RETURNING id ),
           root_contrib AS (
             INSERT INTO backend.t_contributions (saga_id, parent_id, content)
             VALUES ((SELECT id FROM saga), NULL, '2+2=')
             RETURNING id
           ),
           answers AS (
             INSERT INTO backend.t_contributions (saga_id, parent_id, content)
             VALUES ((SELECT id FROM saga), (SELECT id FROM root_contrib), '4'),
                    ((SELECT id FROM saga), (SELECT id FROM root_contrib),
                     '11 (ternary numbering system)')
             RETURNING id
           )
      SELECT id FROM saga
      UNION ALL
      SELECT id FROM root_contrib
      UNION ALL
      SELECT id FROM answers
    `);

    let answer_2 = db_data.rows.pop().id;
    let answer_1 = db_data.rows.pop().id;
    let root_contribution_id = db_data.rows.pop().id;
    let saga_id = db_data.rows.pop().id;

    let response = await request(
      `${api_base_url}/branches`,
      { method: 'GET' }
    );

    assert.equal(response.statusCode, 200);
    assert.equal(JSON.parse(response.body).length, 3);
    assert.deepEqual(
      JSON.parse(response.body),
      [
        {
          parent_tail: null,
          head: root_contribution_id,
          tail: root_contribution_id,
          contribution_count: 1,
          saga_id: saga_id,
          head_content: '2+2=',
          contribution_ids: [root_contribution_id],
          contribution_contents: ['2+2=']
        },
        {
          parent_tail: root_contribution_id,
          head: answer_1,
          tail: answer_1,
          contribution_count: 1,
          saga_id: saga_id,
          head_content: '4',
          contribution_ids: [answer_1],
          contribution_contents: ['4']
        },
        {
          parent_tail: root_contribution_id,
          head: answer_2,
          tail: answer_2,
          contribution_count: 1,
          saga_id: saga_id,
          head_content: '11 (ternary numbering system)',
          contribution_ids: [answer_2],
          contribution_contents: ['11 (ternary numbering system)']
        }
      ]
    );

    await query(`
      DELETE FROM backend.t_contributions WHERE saga_id = ${saga_id};
      DELETE FROM backend.t_sagas WHERE id = ${saga_id};
    `);
  }
]
