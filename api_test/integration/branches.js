require ('../test_helper.js')

module.exports = [
  async function test_branches_flow() {
    first_saga = {
      id: null,
      contributions: []
    };
    second_saga = {
      id: null,
      contributions: []
    };
    let response = await request(
      `${api_base_url}/sagas`,
      {
        method: 'POST',
        data: { title: 'Best buds' }
      }
    );

    assert.equal(response.statusCode, 201);
    first_saga.id = Number(response.headers.location.match(/id=eq\.(\d+)/)[1]);

    response = await request(
      `${api_base_url}/branches`
    );

    assert.equal(response.statusCode, 200);
    assert.deepEqual(JSON.parse(response.body), []);

    response = await request(
      `${api_base_url}/contributions`,
      {
        method: 'POST',
        data: {
          saga_id: first_saga.id,
          content: 'Once upon a time there were two best buds.'
        }
      }
    );

    assert.equal(response.statusCode, 201);
    first_saga.contributions.push(Number(response.headers.location.match(/id=eq\.(\d+)/)[1]));

    response = await request(
      `${api_base_url}/branches?saga_id=eq.${first_saga.id}`,
      { method: 'GET' }
    );

    assert.equal(response.statusCode, 200);
    assert.deepEqual(
      JSON.parse(response.body),
      [
        {
          parent_tail: null,
          head: first_saga.contributions[0],
          tail: first_saga.contributions[0],
          contribution_count: 1,
          saga_id: first_saga.id,
          head_content: 'Once upon a time there were two best buds.',
          contribution_ids: [first_saga.contributions[0]],
          contribution_contents: ['Once upon a time there were two best buds.']
        }
      ]
    );

    response = await request(
      `${api_base_url}/sagas`,
      {
        method: 'POST',
        data: { title: 'English breakfast' }
      }
    );

    assert.equal(response.statusCode, 201);
    second_saga.id = Number(response.headers.location.match(/id=eq\.(\d+)/)[1]);

    response = await request(
      `${api_base_url}/branches?saga_id=eq.${second_saga.id}`,
      { method: 'GET' }
    );

    assert.equal(response.statusCode, 200);
    assert.deepEqual(JSON.parse(response.body), []);

    response = await request(
      `${api_base_url}/contributions`,
      {
        method: 'POST',
        data: {
          saga_id: first_saga.id,
          parent_id: first_saga.contributions[0],
          content: ' They were chilling.'
        }
      }
    );

    assert.equal(response.statusCode, 201);
    first_saga.contributions.push(Number(response.headers.location.match(/id=eq\.(\d+)/)[1]));

    response = await request(
      `${api_base_url}/branches?saga_id=eq.${first_saga.id}`,
      { method: 'GET' }
    );

    assert.equal(response.statusCode, 200);
    assert.deepEqual(
      JSON.parse(response.body),
      [
        {
          parent_tail: null,
          head: first_saga.contributions[0],
          tail: first_saga.contributions[1],
          contribution_count: 2,
          saga_id: first_saga.id,
          head_content: 'Once upon a time there were two best buds.',
          contribution_ids: first_saga.contributions,
          contribution_contents: [
            'Once upon a time there were two best buds.',
            ' They were chilling.'
          ]
        }
      ]
    );

    response = await request(
      `${api_base_url}/branches?saga_id=eq.${second_saga.id}`,
      { method: 'GET' }
    );

    assert.equal(response.statusCode, 200);
    assert.deepEqual(JSON.parse(response.body), []);

    await query(`
      DELETE FROM backend.t_contributions WHERE id IN (${first_saga.contributions.join(',')});
      DELETE FROM backend.t_sagas WHERE id IN (${first_saga.id}, ${second_saga.id});
    `);
  }
]
