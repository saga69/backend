require ('../test_helper.js')
var fs = require('fs');

module.exports = [
  async function test_contributions_flow() {
    let token = fs.readFileSync('../jwt', 'utf8');
    let notify_count = 0;
    let ws = new WebSocket(`${ws_base_url}/contributions/${token}`);
    ws.onerror = function(e) {
      console.log(e);
      assert.fail();
    }
    ws.onopen = function(_) {
      assert(true);
    }
    ws.onmessage = function(message) {
      notify_count++;
      assert.equal(JSON.parse(message.data).payload, 'new contribution');
      assert.equal(JSON.parse(message.data).channel, 'contributions');
    }

    let res = await request(`${api_base_url}/contributions`, { method: 'GET' });
    assert.equal(res.statusCode, 200);
    assert.deepEqual(JSON.parse(res.body), {});

    let saga_data = await query(`
      INSERT INTO backend.t_sagas (title) VALUES ('Hello, friend!')
      RETURNING id
    `);


    res = await request(
      `${api_base_url}/contributions`,
      {
        method: 'POST',
        data: { content: 'yo, ', saga_id: saga_data.rows[0].id }
      }
    );
    assert.equal(res.statusCode, 201);
    let new_id = res.headers.location.match(/.id=eq\.(\d+)/)[1];

    res = await request(
      `${api_base_url}/contributions?id=eq.${new_id}`,
      {
        method: 'GET',
        headers: { accept: 'application/vnd.pgrst.object+json' }
      }
    );
    assert.equal(res.statusCode, 200);
    assert.equal(JSON.parse(res.body).id, new_id);
    assert.equal(JSON.parse(res.body).content, 'yo, ');
    assert.equal(JSON.parse(res.body).parent_id, null);
    assert.equal(JSON.parse(res.body).saga_id, saga_data.rows[0].id);

    await query(`
      DELETE FROM backend.t_contributions WHERE id = ${new_id};
      DELETE FROM backend.t_sagas WHERE id = ${saga_data.rows[0].id}
    `);

    ws.close();
    assert.equal(notify_count, 1);
  }
]
