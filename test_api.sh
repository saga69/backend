cd api_test;
npm install --silent 1>/dev/null;
node run.js;
if [ $? -eq 0 ]
then
  echo 'api tests pass!';
else
  echo 'api tests did not pass...';
  false
fi;
