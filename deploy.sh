server=15.222.13.145
db_port=5432
saga_dir=/home/ubuntu/saga

start_stage() {
  echo "! --> $1"
}
report() {
  local ec=`echo $?`
  if [ $ec -eq 0 ]
  then
    echo '! --> success!'
  else
    echo "! --> exit code: $ec"
  fi
}
deploy() {
  sudo docker ps --format "{{.Ports}}" | grep "0.0.0.0:$db_port" > /dev/null
  if [ $? -eq 0 ]
  then
    echo "some container is hogging port $db_port, which may interfere with database changes deployment"
    echo "please stop any containers before proceeding"
    return
  fi
  start_stage 'copying docker-compose.yml and .env'
  scp .env docker-compose.yml $server:$saga_dir
  report

  start_stage 'starting docker containers'
  ssh $server "cd ${saga_dir} && sudo docker-compose up -d"
  report

  start_stage 'deploying database changes'
  tmux new-session -d -s db_deployment "ssh -L $db_port:localhost:$db_port $server"
  sleep 1 # let the session get established
  sqitch deploy local
  report
  tmux kill-session -t db_deployment
}
deploy
